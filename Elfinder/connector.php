<?php
session_start();
include "../include/connexion.php";
include "../include/sql.php";
error_reporting(0); // Set E_ALL for debuging

include_once dirname(__FILE__).DIRECTORY_SEPARATOR.'elFinderConnector.class.php';
include_once dirname(__FILE__).DIRECTORY_SEPARATOR.'elFinder.class.php';
include_once dirname(__FILE__).DIRECTORY_SEPARATOR.'elFinderVolumeDriver.class.php';
include_once dirname(__FILE__).DIRECTORY_SEPARATOR.'elFinderVolumeLocalFileSystem.class.php';
// Required for MySQL storage connector
// include_once dirname(__FILE__).DIRECTORY_SEPARATOR.'elFinderVolumeMySQL.class.php';
// Required for FTP connector support
// include_once dirname(__FILE__).DIRECTORY_SEPARATOR.'elFinderVolumeFTP.class.php';


/**
 * Simple function to demonstrate how to control file access using "accessControl" callback.
 * This method will disable accessing files/folders starting from  '.' (dot)
 *
 * @param  string  $attr  attribute name (read|write|locked|hidden)
 * @param  string  $path  file path relative to volume root directory started with directory separator
 * @return bool|null
 **/
function access($attr, $path, $data, $volume) {
	return strpos(basename($path), '.') === 0       // if file/folder begins with '.' (dot)
		? !($attr == 'read' || $attr == 'write')    // set read+write to false, other (locked+hidden) set to true
		:  null;                                    // else elFinder decide it itself
}

$upms = getUpms($_SESSION['user_id']);





//version local, car on n'envoie pas les clusters dans le rendu
$opts = array(
 'debug' => true,
	'roots' => array(
		array(
			'driver'        => 'LocalFileSystem',   // driver for accessing file system (REQUIRED)
			'path'          => '../data1/'.$_SESSION['user_id']."-".$_SESSION['user_mail'] ,         // path to files (REQUIRED)
			'URL'           => 'http://localhost/cubbyhole/data1/'.$_SESSION['user_id']."-".$_SESSION['user_mail'], // URL to files (REQUIRED)
			'accessControl' => 'access' ,          // disable and hide dot starting files (OPTIONAL)
			'uploadMaxSize' => $upms.'M'

		)
	)
);

/* version server 
$opts = array(
 'debug' => true,
	'roots' => array(
		array(
			'driver'        => 'LocalFileSystem',   // driver for accessing file system (REQUIRED)
			'path'          => '//192.168.1.101/data1/'.$_SESSION['user_id']."-".$_SESSION['user_mail'] ,         // path to files (REQUIRED)
			'URL'           => 'http://192.168.1.101/data1/'.$_SESSION['user_id']."-".$_SESSION['user_mail'], // URL to files (REQUIRED)
			'accessControl' => 'access' ,          // disable and hide dot starting files (OPTIONAL)
			'uploadMaxSize' => '100M'

		)
	)
);
*/

// run elFinder
$connector = new elFinderConnector(new elFinder($opts));
$connector->run();

