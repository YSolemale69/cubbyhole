<?php
session_start();

include "include/connexion.php";

if(isset($_GET['id']) && isset($_GET['p']))
{
	//SI CEST UN LIEN PRIVE
	if(isset($_GET['p']) && $_GET['p']=="prive")
	{
		$isHim=false;
		$req = $bdd->prepare('SELECT id_owner FROM lien_prive WHERE id_owner= :id ');
		$req->execute(array(
			'id' =>$_SESSION['user_id']));			 
		while ($donnees = $req->fetch())
		{
			$isHim=true;
		}

		$req->closeCursor(); 
		
		if($isHim==true)
		{
			$req = $bdd -> prepare ('DELETE FROM lien_prive WHERE id =:id ');
			$req -> execute (array("id"=>$_GET["id"]));
		}
	}
	
	
	//SI CEST UN LIEN PUBLIC
	if(isset($_GET['p']) && $_GET['p']=="public")
	{
		$isHim=false;
		$req = $bdd->prepare('SELECT id_owner FROM lien_public WHERE id_owner= :id ');
		$req->execute(array(
			'id' =>$_SESSION['user_id']));			 
		while ($donnees = $req->fetch())
		{
			$isHim=true;
		}

		$req->closeCursor(); 
		
		if($isHim==true)
		{
			$req = $bdd -> prepare ('DELETE FROM lien_public WHERE id =:id ');
			$req -> execute (array("id"=>$_GET["id"]));
		}
	}
	
		header('Location: partage.php?info=deleteSuccess');

}
else{header('Location: partage.php');}

?>