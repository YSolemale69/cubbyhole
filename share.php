
<?php
session_start();
include "include/connexion.php";
include "include/sql.php";


$tabLink=null;
if(isset($_GET['p']) && isset($_GET['id']))
{
	$tabLink = getLinkById($_GET['p'], $_GET['id']);
	
	$mailOwner = getUserById($tabLink[0]['id_owner']);
}

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="css/style1.css" />
        <link rel="icon" type="image/png" href="img/favicon.png" />
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" type="text/css" />
		<title>Mes liens de partage</title>
</head>

	
<body id="corpus">
	<?php include "include/header.php";?>
	<div class="row-fluid">
		<div class="span12">

	
	<div id="corp" style="color:white;">
		
		<div id="mainContent">
		
			<?php 
			if($tabLink!=null)
			{
				?>
				
					<h1> Fichier Partagé </h1>
					<p> Visibilité : <?php echo $_GET['p'];?></p>
					<p> Propriétaire : <?php echo $mailOwner ?></p>
					
					<?php

					$tabLink[0]['url']=str_replace('\\','/',$tabLink[0]['url']);
					
					$ext = substr($tabLink[0]['url'], -3);
					
					
					if($ext=="jpg" ||$ext=="png" ||$ext=="gif" ||$ext=="pmb" ||$ext=="jpeg")
					{ ?>
						<img src="data1/<?php echo $_GET['user_id']."-".$_GET['mail'] ?>/<?php echo $tabLink[0]['url'];?>" alt="image de partage" style="border-radius:5px;border:1px solid black; max-width:800px;"/>
						<p><a href="data1/<?php echo $_GET['user_id']."-".$_GET['mail'] ?>/<?php echo $tabLink[0]['url'];?>" /> Image taille réelle </a></p>
					<?php
					}

					else if($ext=="mp3" ||$ext=="ogg")
					{?>
						<audio controls autoplay>
							<?php 
							if($ext=="mp3"){echo '<source src="data1/'.$_GET['user_id'].'-'.$_GET['mail'].'/'.$tabLink[0]['url'].'" type="audio/mpeg">';}
							else{echo  '<source src="data1/'.$_GET['user_id'].'-'.$_GET['mail'].$tabLink[0]['url'].'" type="audio/ogg">';}?>
						  
							Your browser does not support the audio element.
						</audio> 
						<p><a href="data1/<?php echo $_GET['user_id']."-".$_GET['mail'] ?>/<?php echo $tabLink[0]['url'];?>" />  Télécharger : Clique droit -> Enregister la cible de l'ellement  </a></p>
						<?php
					}

					else if( $ext=="wmv" || $ext=="flv" || $ext=="mpg" || $ext=="avi" ||$ext=="mp4")
					{
						if($ext=="mp4")
						{?>
							<video width="600" height="400" controls autoplay>
								<?php 
								if($ext=="mp4"){echo '<source src="data1/'.$_GET['user_id'].'-'.$_GET['mail'].$tabLink[0]['url'].'" type="video/mp4">';}
								
								?>
							  
								Your browser does not support the VIDEO element.
							</video> <?php
						}?>
						<p><a href="data1/<?php echo $_GET['user_id']."-".$_GET['mail'] ?>/<?php echo $tabLink[0]['url'];?>" />  Télécharger : Clique droit -> Enregister la cible de l'ellement  </a></p>
						<?php
					}

					else
					{ ?>
					<p> <a href="data1/<?php echo $_GET['user_id']."-".$_GET['mail'] ?>/<?php echo $tabLink[0]['url'];?>"> Télécharger le fichier </a></p>
					
					<?php
					}
					?>

			<?php	
			}
			else
			{?>
				<h2> Nous somme désolé, le fichier que vous tentez de visualiser est introuvable </h2>
				<?php
			}
			?>
			
		</div>
		
		</div>

		</div>
	</div>

<br><br>

<footer>
	<?php include "include/footer.php";?>
</footer>

</body>

</html>