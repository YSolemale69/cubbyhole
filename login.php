<?php
session_start();

include "include/connexion.php";

//si déjà connecté
if(isset($_SESSION['user_id']))
{
	header('Location: index.php');
}

//vérification connexion
if(isset($_POST["mail_user"]) || isset($_POST["password_user"]))
{

	$userID=0;
	$pass_hache = sha1($_POST["password_user"]);
	
	include_once "include/sql.php";
	$isHere=getUserByMail($_POST['mail_user']);

	//si l'user existe on récupère ses infos
	if ($isHere) 
	{
		$data=getUserInfoByMail($_POST['mail_user']);
		var_dump($data);

		$userID= $data['id_user'];
		$userPass=$data['password'];
		$userMail=$data['mail'];
		$userExpplan=$data['exp_plan'];
	}
	
	//si connexion acceptée
	if( $userPass == $pass_hache)
	{
		$_SESSION['user_id'] = $userID;
		$_SESSION['user_mail'] = $userMail;

		//vérifie date d'expiration
		$curentdate = date('Y-m-d', time());
		
		if ($userExpplan == $curentdate) 
		{
			include_once "include/sql.php";
			setPlan($userID, 1);
		}

		header('Location: profil.php');
	}
	else
	{
		$error = "Votre email ou votre mot de passe est invalide";
	}
}

if(isset($error))
{
	echo "<h2>".$error."</h2>";
}

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="css/style1.css" />
        <link rel="icon" type="image/png" href="img/favicon.png" />
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" type="text/css" />
		<title>Connexion</title>
</head>

	
	<body id="corpus">
		<?php include "include/header.php";?>
		<div class="row-fluid">
			<div class="span4 offset4">

				<h2>Connexion</h2>
				<form enctype="multipart/form-data" action="#" method="post">
					<p><input type="text" name="mail_user" placeholder="E-mail" required/></p>
					<p><input type="password" name="password_user" placeholder="Mot de passe" required/></p>
					<p><button type="submit" class="btn btn-success" name="sendLogin" value="Valider"> Valider</button></p>
				</form>
				Pas encore inscrit? <a href="inscription.php"> cliquez ici </a>

			</div>
		</div>

	<br><br>

		<footer>
			<?php include "include/footer.php";?>
		</footer>

	</body>

</html>