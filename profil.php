
<?php
session_start();
include "include/connexion.php";
include "include/sql.php";

if(!isset($_SESSION['user_id']))
{
	header('Location: index.php?exit=needLogin');
}	

$planUser = getPlan($_SESSION['user_id']);

//on calcule la capacité et la taille des fichiers 
$total_size = foldersize("files/".$_SESSION['user_id']); 
$myCap = format_size2($total_size, 1 ,$planUser['stockage']);
?> 

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="css/style1.css" />
        <link rel="icon" type="image/png" href="img/favicon.png" />
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" type="text/css" />
		<title>Profil</title>
</head>

	
<body id="corpus">

	<?php include "include/header.php";?>

	<div class="row-fluid">
		<div class="span12"> 

				
			<h1 style="text-align:center;"> Profil</h1>
		
			
			<div style="width:700px;margin:0px auto 60px auto">

				<section id="info_user" class="dp50">
				
					<h2><strong>Infos : </strong></h2>
					<p>E-mail : <?php echo $_SESSION['user_mail'];?> </p>
					<p>Mot de passse : ******</p>
					
				</section>
				
				
				<section id="espace_user" class="dp50">
				
					<h2><strong>Espace : </strong></h2>
					<?php  

						$total_size = foldersize("files/".$_SESSION['user_id']);
						echo format_size ($total_size, 2 ) ." sur ". $planUser['stockage'] . "Go  "; 	
						if($myCap>84)
						{
							echo "  <img src='img/warning_icon.png' title='moins de 15% d espace restant' alt='ATTENTION' />";
						}
					?>
						
					<div id="capacityBar" class="progress" >
						<div id="capacityBarR" class="bar bar-success" title="<?php echo $myCap.'%';?>" style="padding-left:5px; width: <?php echo $myCap.'%';?>">
							<span style="margin-right:auto;margin-left:auto;"><?php echo $myCap.'%';?></span>
						</div>
					</div>
					
				</section>

				<div class="clear"></div>
			</div>
				
			
				
			<div class="clear"></div>
			
			<hr style="width:80%; margin:10px auto 20px auto;"></hr>
			
			
			<section id="plan_user">

				<h2><strong>Plan :  </strong></h2>
				<div class="row-fluid">

					<div class="span3">
						<p><u>Type</u> : <?php if($planUser['name'])echo $planUser['name']; ?></p>
						<p><u>Prix / an </u> : <?php echo $planUser['prix_an']; ?> € </p>
					</div>
					
					<div class="span4">
						<?php
						 if($planUser['name']=="Gratuit"){ echo '<img src="img/logo/Drive_Download.png" alt="logo"/>'; }
						 if($planUser['name']=="Professionnel"){ echo '<img src="img/logo/Suit.png" alt="logo"/>';  }
						 if($planUser['name']=="Premium"){ echo '<img src="img/logo/Premium.png" alt="logo"/>';  }
						 if($planUser['name']=="Custom"){ echo '<img src="img/logo/Setup.png" alt="logo"/> '; }
						 ?>
					</div>
					
					<div class="span4">
						<p><u>Vitesse d'upload</u> : <?php echo $planUser['upload']; ?>Mo/s </p>
						<p><u>Vitesse de download</u> : <?php echo $planUser['download']; ?>Mo/s </p>
					</div>

				</div>
				
				
				<div class="clear"></div>
			</section>
				
			
			</div>
			
	</div>

<br></br><br></br><br></br>

<footer>
	<?php include "include/footer.php";?>
</footer>

</body>

</html>