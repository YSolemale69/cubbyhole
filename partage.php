
<?php
session_start();
include "include/connexion.php";
include "include/sql.php";

if(!isset($_SESSION['user_id']))
{
	header('Location: index.php?exit=needLogin');
}

$tabPrivateLink = getAllPrivateLink();
$tabPublicLink = getAllPublicLink();


?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="css/style1.css" />
        <link rel="icon" type="image/png" href="img/favicon.png" />
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" type="text/css" />
		<title>Partages</title>
</head>

	
<body id="corpus">
	<?php include "include/header.php";?>
	<div class="row-fluid">
		<div class="span12">

	
	<div id="corp" style="color:white;">
		
		<div id="mainContent">
			
			<h1> Vos Partages </h1>
			
			<?php
				if(isset($_GET['info'] )&& $_GET['info']=="createSuccess")
				{?>
				<div  id="InfoDiv" class="bandeauPaiement bggreen" >
					 Votre Lien à bien été créé
				</div>
				<?php
				}
				if(isset($_GET['info']) && $_GET['info']=="deleteSuccess")
				{?>
				<div  id="InfoDiv" class="bandeauPaiement bggreen" >
					 Votre Lien à bien été supprimé
				</div>
				<?php
				}
				?>
			
			<section>
				<h2> Liens Privés </h2>
				<ul>
					<?php
					$foo=0;
					foreach ($tabPrivateLink as $cle => $valeur)
					{
						if($valeur["id_owner"]==$_SESSION['user_id'])
						{
							echo '<li>Lien : 
							<a href="share.php?p=prive&id='.$valeur["id"].'&user_id='.$valeur['id_owner'].'&mail='.$valeur['mail_user_cible'].'">
								cubbyhole/share.php?p=prive&id='.$valeur["id"].'&user_id='.$valeur['id_owner'].'&mail='.$valeur['mail_user_cible'].' 
							</a> 
							<a href="deleteLinkById.php?p=prive&id='.$valeur["id"].'" > 
								<img src="img/delete_link.png" width="30" />
							</a>
							</br>
							<i>Redirige vers : "'.$valeur["url"].'"</i>
							</br>
							Partagé avec  : '.$valeur["mail_user_cible"].'
							</li>';

							$foo++;
						}
					}

					if($foo==0){echo "<p> Vous n'avez pas de lien privé</p>";} 
					?>
					
					
				</ul>

			</section>

			<hr width="50%" style="margin-left:25%" ></hr>
			
			<section>
				<h2> Liens Publics </h2>	
				<ul>
					
					
					<?php
					$foo2=0;
					foreach ($tabPublicLink as $cle => $valeur)
					{
						if($valeur["id_owner"]==$_SESSION['user_id'])
						{
							echo '<li>Lien : 
							<a href="share.php?p=public&id='.$valeur["id"].'&user_id='.$_SESSION['user_id'].'&mail='.$_SESSION['user_mail'].'">
								cubbyhole/share.php?p=prive&id='.$valeur["id"].' 
							</a>  
							<a href="deleteLinkById.php?p=public&id='.$valeur["id"].'" > 
								<img src="img/delete_link.png" width="30" />
							</a>
							</br>
							<i>Redirige vers : "'.$valeur["url"].'"
							</i>
							</li>';
							$foo2++;
						}
					}

					if($foo2==0){ echo"<p> Vous n'avez pas de lien public</p>";}
					?>
				</ul>	
			
			</section>
			
		
		</div>

		</div>
	</div>

<br></br><br></br><br></br><br></br>

<footer>
	<?php include "include/footer.php";?>
</footer>

</body>

</html>