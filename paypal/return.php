<?php
session_start();
include("fonction_api.php"); 
$requete = construit_url_paypal(); 

// On ajoute le reste des options
$requete = $requete."&METHOD=DoExpressCheckoutPayment".
			"&TOKEN=".htmlentities($_GET['token'], ENT_QUOTES). 
			"&AMT=".$_SESSION['amt'].
			"&CURRENCYCODE=EUR".
			"&PayerID=".htmlentities($_GET['PayerID'], ENT_QUOTES). 
			"&PAYMENTACTION=sale";

$ch = curl_init($requete);

// Modifie l'option CURLOPT_SSL_VERIFYPEER afin d'ignorer la vérification du certificat SSL. Si cette option est à 1, une erreur affichera que la vérification du certificat SSL a échoué, et rien ne sera retourné
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

// Retourne directement le transfert sous forme de chaîne de la valeur retournée par curl_exec() au lieu de l'afficher directement
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

$resultat_paypal = curl_exec($ch);

if (!$resultat_paypal) 
	{echo "<p>Erreur</p><p>".curl_error($ch)."</p>";}
else
{
	$liste_param_paypal = recup_param_paypal($resultat_paypal);
	
	if ($liste_param_paypal['ACK'] == 'Success')
	{
		echo '
		<html>
		    <head>
		        <meta charset="utf-8" />
		        <link rel="stylesheet" href="../css/style1.css" />
		        <link rel="icon" type="image/png" href="../img/favicon.ico" />
		        <link rel="stylesheet" href="http://likenco.com/css/bootstrap/css/bootstrap.min.css" type="text/css" />
				<title>Récapitulatif</title>
		</head>

	
		<body id="corpus">
			 <?php //include "../include/header.php";?>
			
					<div class="row-fluid">
						<div class="span10 offset1">';

		echo "<h1>Le paiement a été effectué</h1>";
		echo '<div class="alert alert-success">
				<h4 class="alert-heading">Félicitation !</h4>
				Votre compte a bien été enregistré
				</div>';
		echo "<br><h2>Récapitulatif:</h2>";
		
		include "../include/sql.php";

		if ($_SESSION['amt'] == 50.00)
    	{
    		echo "<br><h3 style='text-align: left;'>Abonnement 1 an - Compte Professionel</h3>";

			echo "<br><h3 style='text-align: left;'>Total TTC : 50.00€</h3>";

			
			setPlan($_GET['user_id'], 2);
    	}
    
    	elseif ($_SESSION['amt'] == 100.00) 
    	{
    		echo "<br><h3 style='text-align: left;'>Abonnement 1 an - Compte Premium</h3>";

			echo "<br><h3 style='text-align: left;'>Total TTC : 100.00€</h3>";

			setPlan($_GET['user_id'], 3);
    	}

		echo '	
		<br><a href="../index.php"> cliquez ici </a>	
		</div>
		</div>
			
			<br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br>
			<p class="lienAncre" style="text-align:center;font-size:12px;"></p>
			
			
			<hr width="90%" style="color:#96d5fc; margin-left:3%;">
		
		
		<footer>';
		
		include "../include/footer.php";
			
		echo'
		</footer>

		</body>

		</html>';

		
	}
	else 
	{echo "<p>Erreur de communication avec le serveur PayPal.<br />".$liste_param_paypal['L_SHORTMESSAGE0']."<br />".$liste_param_paypal['L_LONGMESSAGE0']."</p>";}
}
curl_close($ch);
?>