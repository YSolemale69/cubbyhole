<h1>Paiement annulé</h1>
<br>

<h3 style='text-align: left;'>
Le paiement a été annulé. En espérant que vous changerez d'avis, nous vous adressons nos salutations les plus sincères.
</h3>
<br>

<div class="alert alert-warning">
	<h4 class="alert-heading">Attention !</h4>
	Suite à votre annulation, votre compte n'a pas été changé
</div>
<br>