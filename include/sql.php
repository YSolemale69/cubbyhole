<?php
function getUserByMail($mail)
{
	include "connexion.php";
	
	$isHere=false; 

	$req = $bdd->prepare('SELECT mail FROM users WHERE mail = :mail ');
	$req->execute(array(
	'mail' => $mail
	));
				 
	while ($data = $req->fetch())
	{
		$foo = $data['mail'];
		$isHere=true;
	}

	$req->closeCursor(); 
		
	return $isHere;
}

function getUserInfoByMail($mail)
{
	include "connexion.php";
	
	$req = $bdd->prepare('SELECT * FROM users WHERE mail = :mail ');
	$req->execute(array(
	'mail' => $mail
	)); 
		
	return $req->fetch();
}

function getPlan($userID2)
{
	include "connexion.php";

	$req = $bdd->prepare('SELECT id_plan FROM users WHERE id_user = :id');
	$req->execute(array('id' => $userID2));	
	while ($donnees = $req->fetch())
	{
		$id_plan= $donnees['id_plan'];
	}

	$req->closeCursor(); 
	
	
	$req = $bdd->prepare('SELECT * FROM plan WHERE id_plan = :id');
	$req->execute(array('id' => $id_plan));	
	while ($donnees = $req->fetch())
	{
		$infoPlan = array(
		'id_plan' => $donnees['id_plan'],
		'prix_an' => $donnees['prix_an'],
		'name' => $donnees['name'],
		'stockage' => $donnees['stockage'],
		'maxShare' => $donnees['maxShare'],
		'upload' => $donnees['upload'],
		'download' => $donnees['download']
		);
	}

	$req->closeCursor(); 

	return $infoPlan ;
}

function getidPlan($uid)
{
	include "connexion.php";
	
	$plan=0;
	
	$req = $bdd->prepare('SELECT id_plan FROM users WHERE id_user = ?');
	$req->execute(array($uid));	
	while ($data = $req->fetch())
	{
		$plan = $data['id_plan'];
	}

	$req->closeCursor(); 

	return $plan ;
}

function setPlan($user, $plan)
{
    include "connexion.php";
                              
    $req = $bdd -> prepare ('UPDATE users SET id_plan = ? WHERE id_user = ?');
    $req -> execute (array(
    $plan,
    $user));
  
    $req->closeCursor();
    return true;
} 

function setInscription($email, $password)
{
	include "connexion.php";
	
	$pass_hache = sha1($password);

	$nextYear = time() + (365 * 24 * 60 * 60);
	$nextYear =  date('Y-m-d', $nextYear);

	
	$req = $bdd -> prepare ('INSERT INTO users(mail, password, exp_plan) VALUES(?, ?, ?)');
	$req -> execute (array(
	$email,  
	$pass_hache,
	$nextYear));

	
	$req = $bdd->query('SELECT * FROM users ORDER BY id_user DESC LIMIT 1');
	while ($donnees = $req->fetch())
	{
		
		$_SESSION['user_id'] = $donnees['id_user'];
		$_SESSION['user_mail'] = $donnees['mail'];
	}

	$req->closeCursor(); 
	
	return true;

}


function getAllUser()
{
	include "connexion.php";
	
	$tabUser=array();
	$req = $bdd->query('SELECT * FROM users');
	$req->execute();	
			 
	while ($data = $req->fetch())
	{
		array_push($tabUser,array(
			"id_user"=>$data['id_user'],
			"mail_user"=>$data['mail'],
			"id_plan" => $data['id_plan']
			)); 
	}

	$req->closeCursor(); 
		
	return $tabUser;
}

function getUpms($uid)
{
	$plan = getidPlan($uid);
	
	include "connexion.php";
	
	$req = $bdd->prepare('SELECT upload FROM plan WHERE id_plan = ?');
	$req->execute(array(
	$plan
	));	
	
	while ($data = $req->fetch())
	{
		$upms = $data['upload'];
	}
	
	$req->closeCursor(); 
			
	return $upms;
}

function foldersize($rep) 
{
	$racine=@opendir($rep);
    $taille=0;
    while($dossier=@readdir($racine))
    {
      if(!in_array($dossier, array("..", ".")))
      {
        if(is_dir("$rep/$dossier"))
        {
          $taille+=foldersize("$rep/$dossier");
        }
        else
        {
          $taille+=@filesize("$rep/$dossier");
        }
      }
    }
    @closedir($racine);
    return $taille;
  }

function taille_dossier1($rep)
{
    $racine=@opendir($rep);
    $taille=0;
    $dossier=@readdir($racine);
    $dossier=@readdir($racine);
    while($dossier=@readdir($racine))
    {
       
       if(is_dir("$rep/$dossier"))
       	{
          $taille+=foldersize("$rep/$dossier");
       	}
	    else
	    {
	      $taille+=@filesize("$rep/$dossier");
	    }
    }

    @closedir($racine);
    return $taille;
}

function format_size($size , $round)
{
	
	$sizes = array(' Octet(s)', 'ko', 'Mo', 'Go', 'To', 'Po', 'Eo', 'Zo', 'Yo');
	for ($i=0; $size > 1024 && $i < count($sizes) - 1; $i++) $size /= 1024;
	return round($size,$round).$sizes[$i];
} 

function format_size2($size , $round, $capacite)
{
	
	$sizes = array(' Octet(s)', 'ko', 'Mo', 'Go', 'To', 'Po', 'Eo', 'Zo', 'Yo');
	for ($i=0; $size > 1024 && $i < count($sizes) - 1; $i++) $size /= 1024;
	
	//on formate la capacit� en GO
	if($i==0){$capacite*=1000000000;}
	if($i==1){$capacite*=1000000;}
	if($i==2){$capacite*=1000;}	
	
	$percent = $size/$capacite*100;
	
	return (round($percent,$round));
}

function format_capacity($size , $round)
{

}

function getAllPrivateLink()
{
	include "connexion.php";
	
	$tabLink=array();
	$req = $bdd->query('SELECT * FROM lien_prive');
			 
	while ($donnees = $req->fetch())
	{
		array_push($tabLink,array(
			"id"=>$donnees['id'],
			"url"=>$donnees['url_fichier'],
			"id_owner" => $donnees['id_owner'],
			"mail_user_cible" => $donnees['mail_user_cible']
			)); 
	}

	$req->closeCursor();
		
	return $tabLink;
}


function getAllPublicLink()
{
	include "connexion.php";
	
	$tabLink=array();
	$req = $bdd->query('SELECT * FROM lien_public');
			 
	while ($donnees = $req->fetch())
	{
		array_push($tabLink,array(
			"id"=>$donnees['id'],
			"url"=>$donnees['url_fichier'],
			"id_owner" => $donnees['id_owner']
			)); 
	}

	$req->closeCursor(); 
		
	return $tabLink;
}

function getLinkById($type, $id)
{
	include "include/connexion.php";
		
	$tabLink=array();

	if(isset($type) && $type=="prive")
	{
		$isHim=false;
		$req = $bdd->prepare('SELECT * FROM lien_prive WHERE id= :id ');
		$req->execute(array(
			'id' =>$id));			 
		while ($donnees = $req->fetch())
		{
			array_push($tabLink,array(
				"id"=>$donnees['id'],
				"url"=>$donnees['url_fichier'],
				"id_owner" => $donnees['id_owner'],
				"mail_user_cible" => $donnees['mail_user_cible']
				)); 
		}
		$req->closeCursor(); 
		
		return $tabLink;
	}
	
	
	else if(isset($type) && $type=="public")
	{
		$isHim=false;
		$req = $bdd->prepare('SELECT * FROM lien_public WHERE id= :id ');
		$req->execute(array(
			'id' =>$id));			 
		while ($donnees = $req->fetch())
		{
			array_push($tabLink,array(
				"id"=>$donnees['id'],
				"url"=>$donnees['url_fichier'],
				"id_owner" => $donnees['id_owner']
				)); 
		}
		$req->closeCursor();
		
		return $tabLink;
	}

	else
	{
		return null;
	}
	
}

function getUserById($id)
{
		include "include/connexion.php";
		
		$foo=null; 

		$req = $bdd->prepare('SELECT mail FROM users WHERE id_user = :id ');
		$req->execute(array('id' => $id));
					 
		while ($donnees = $req->fetch())
		{
			$foo = $donnees['mail'];
		}

		$req->closeCursor();
			
		return $foo;
}

?>