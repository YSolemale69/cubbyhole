<?php 
include "include/connexion.php";
include "include/sql.php";

if(isset($_POST["sendNewPlan"]))
{
	// $isPlanCreate=setPlan();
}

if(isset($_POST["sendRegister"]))
{
	$isUserCreate = setInscription($_POST["email_user"],$_POST["password_user"]);
}

 ?>

    
	
   <!DOCTYPE html>
<html>
    <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta charset="utf-8" />
        <link rel="stylesheet" href="css/style1.css" />
        <link rel="icon" type="image/png" href="img/favicon.png" />
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" type="text/css" />
		<title>Administration</title>
</head>

	
<body id="corpus">
	
			<div class="row-fluid">
				<div class="span11 offset1"><br>
					<h1>Etat des serveurs</h1>

<?php 
	$host = '185.28.20.15'; 
	$port = 80; 
	$waitTimeoutInSeconds = 10; 
	if($fp = fsockopen($host,$port,$errCode,$errStr,$waitTimeoutInSeconds))
	{   
		echo "Serveur principal statut : <span style='color:green;'>OK</span><br>";
	} 

	else 
	{
	   echo "Serveur principal statut : <span style='color:red;'>ERROR</span><br>"; 
	   echo utf8_encode($errStr). "<br>Code d'erreur : (".$errCode.")<br>";
	} 

	fclose($fp);


	$host1 = '192.168.0.1';
	if($fp = fsockopen($host1,$port,$errCode1,$errStr1,$waitTimeoutInSeconds))
	{   
		echo "Serveur secondaire statut : <span style='color:green;'>OK</span><br>";
	} 

	else 
	{
	   echo "Serveur secondaire statut : <span style='color:red;'>ERROR</span><br>"; 
	   echo utf8_encode($errStr1). "<br>Code d'erreur : (".$errCode1.")<br>";
	} 

	fclose($fp);
?>

    <h1>Gestion des Utilisateurs</h1>

	
	<TABLE>
	<tr><th><b>ID User</b></th><th style="padding-right:100px"><b>Mail</b></th>
	<th><b>Password</b></tg><th><b>ID Plan</b></th><th>Supprimer</th></tr>
	<?php
		$req = $bdd->query('SELECT * FROM users ORDER BY id_user DESC');

	while ($donnees = $req->fetch())
	{
		echo '<tr><td style="padding-right:100px">'.$donnees['id_user'].'</td><td>'.$donnees['mail'].'</td><td>'.$donnees['password'].'</td>
				<td>'.$donnees['id_plan'].'</td><td><a href="deleteUserById.php?id='.$donnees["id_user"].'&user='.$donnees["mail"].'">Supprimer</a></td></tr>';
}
	$req->closeCursor(); 
		
	?>
	</table>
	
	Ajout d'un utilisateur :
	
	<form enctype="multipart/form-data" action="<?php echo $_SERVER['PHP_SELF']?>" method="post">
		<p><input type="email" name="email_user" placeholder="E-mail" required /></p>
		<p><input type="password" name="password_user" placeholder="Mot de passe" required></p>
		<p><input type="password" name="password_user_conf" placeholder="Confirmation"  required/></p>
		<p><input id="sendRegister" type="submit" name="sendRegister" value="Valider" /></p>
	</form>
					
<hr width="100%" style="color:#292929;" /> 


	<h1>Gestion des Plans</h1>
	
	<table>
	<tr><td><b>Nom Plan</b></td><td style="padding-right:100px"><b>Stockage</b></td>
	<td><b>Partage</b></td><td><b>Upload</b></td><td><b>Download</b></td><td><b>Prix</b></td></tr>
	
	<?php
		$req = $bdd->query('SELECT * FROM plan ORDER BY id_plan DESC');

	while ($donnees = $req->fetch())
	{
		echo '<tr><td style="padding-right:100px">'.$donnees['name'].'</td><td>'.$donnees['stockage'].'</td><td>'.$donnees['maxShare'].'</td>
		<td>'.$donnees['upload'].'</td><td>'.$donnees['download'].'</td><td>'.$donnees['prix_an'].'</td></tr>';
	}

	$req->closeCursor();
		
		
	
	?>
	</table>
	
Créer un plan : 

		<form id="formNewPlan" enctype="multipart/form-data" action="<?php echo $_SERVER['PHP_SELF']?>" method="post" onsubmit="return(submitform())">		
				<div id="stockage">
				<b>Choissisez votre espace de disque : </b>
				 <input type='number' min="1" max="400" name='stockage' id='plan_stockage' style="width:50px" onchange="hideSend()" required> Go </br><br> 
			</div>
			<div id="bandepassante">
				<b>Choissisez votre bande passante : </b></br><br>
				<label for "upload">Upload : <input type='number' name='upload' id='plan_upload' min='1' max='50' style="width:50px" onchange="hideSend()" required>Mo/s </br>
				<label for "download">Download : </label><input type='number' name='download' id='plan_download' min='1' max='200' style="width:50px" onchange="hideSend()" required>Mo/s</br></br>
			</div>
			<div id="partage">
				<b>Choissisez votre maximum de partage/jours (100 pour illimité) : </b>
				<input type='number' min="0" name='maxShare' id='plan_maxShare' style="width:50px" onchange="hideSend()" required></br>
			</div>
			
			
			<p><strong> Prix :  <span id="prixPlan">0</span>$ / An</strong></p>
			
			<div><input type="radio"id="choixPrixperso" name="choixPrix" value="perso">Prix personnalisé
			<input type="radio" id="choixPrixauto" name="choixPrix" value="auto">Prix automatique</div>
			<input type="number" value="0" id="plan_prix_an" name="prix_an" style="opacity:0"/>
			
			<p class="typeButton" onclick="getPrice()"> Calculer prix </p>
			
			<p  id="sendNewPlan"><input type="submit" name="sendNewPlan" value="Valider" /></p>
		</form>	

		</div>
			
	</div>

<br></br><br></br><br></br>

<footer>
	<?php include "include/footer.php";?>
</footer>

</body>
				
<script type="text/javascript">	

function getPrice()

	{
	if(document.getElementById('choixPrixauto').checked=="true"
	{
		var stockage = document.getElementById('plan_stockage').value;
		var upload = document.getElementById('plan_upload').value;
		var download = document.getElementById('plan_download').value;
		var maxShare = document.getElementById('plan_maxShare').value;
		
		if(stockage=="" || upload=="" || download=="" || maxShare==""){alert ("Vous ne pouvez pas laisser de case vide");}

		
		var prix=0;
		
		if(stockage>5)
		{
			prix+=stockage*0.8;
			
		}
		if(upload>1)
		{
			prix+=upload*1;
		}
		if(download>5)
		{
			prix+=download*0.5;
		}
		if(maxShare>5)
		{
			prix+=maxShare*0.2;
		}
				
		$('#prixPlan').text(prix);
		document.getElementById('plan_prix_an').value=prix;
		}
	
	}
	
	</script>
	
</html>
