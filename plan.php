<?php
session_start();

	if(isset($_SESSION['user_id']))
	{
		
	 	if (isset($_POST['paiement'])) 
	    {
	    	//on lance l'api paypal
	    	include("/paypal/script_api.php");
		}

		if (isset($_POST['gratuit']))
		{
	    	include "include/sql.php";
			setPlan($_SESSION['user_id'], 1);
			
			echo '<br><div class="alert alert-success">
				<h4 class="alert-heading">Félicitation !</h4>
				Votre compte a bien été enregistré <a href="profil.php"> Accéder à mon profil</a>
				</div>';
		}
	}
	else
	{
		header('Location: index.php');
	}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="css/style1.css" />
        <link rel="icon" type="image/png" href="img/favicon.png" />
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" type="text/css" />
		<title>Plans</title>
</head>

	
<body id="corpus">
	
	<div class="row-fluid">
		<div class="span4">
			<img src="img/logo-cloud.png" alt="logo" />
		</div>

		<div class="span8">
			<br><h1>Choix du type de compte</h1>
		</div>
	</div>

		
		<div class="row-fluid" style="padding:1%;">
			<div class="span4">
				<h1>Gratuit</h1>
				<img src="img/logo/Empty_button.png" style="margin-left:7%;"/>
				<br>
				<p style="padding-left: 10%; padding-right: 10%">Prix: Gratuit</p>
				<p style="padding-left: 10%; padding-right: 10%">
					<form enctype="multipart/form-data" action="" method="post">
     				<input type="hidden" name="montant" value="1" />
     				<button type="submit" name="gratuit" class="btn btn-primary" style="margin-left: 5%;" type="button">&nbsp Souscrire <i class="icon-white icon-ok-sign"></i></button>
    				</form>
    			</p>
			</div>

			<div class="span4">
				<h1>Professionnel</h1>
				<img src="img/logo/Suit.png" style="margin-left:7%;"/>
				<br>
				<p style="padding-left: 10%; padding-right: 10%">Prix: 50€/an</p>
				<p style="padding-left: 10%; padding-right: 10%">
					<form enctype="multipart/form-data" action="" method="post">
     				<input type="hidden" name="montant" value="1" />
     				<button type="submit" name="paiement" class="btn btn-primary" style="margin-left: 5%;" type="button">&nbsp Souscrire <i class="icon-white icon-ok-sign"></i></button>
    				</form>
    			</p>
			</div>

			<div class="span4">
				<h1>Premium</h1>
				<img src="img/logo/Premium.png" style="margin-left:7%;"/>
				<br>
				<p style="padding-left: 10%; padding-right: 10%">Prix: 100€/an</p>
				<p style="padding-left: 10%; padding-right: 10%">
					<form enctype="multipart/form-data" action="" method="post">
     				<input type="hidden" name="montant" value="2" />
     				<button type="submit" name="paiement" class="btn btn-primary" style="margin-left: 5%;" type="button">&nbsp Souscrire <i class="icon-white icon-ok-sign"></i></button>
    				</form>
    			</p>
			</div>
		</div>


			
		<br></br><br></br><br></br>
		<p class="lienAncre" style="text-align:center;font-size:12px;"></p>
		
		
		<hr width="90%" style="color:#96d5fc; margin-left:3%;">
	

		<footer>
			<?php include "include/footer.php";?>
		</footer>

</body>

</html>