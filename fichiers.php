
<?php
session_start();
include "include/connexion.php";
include "include/sql.php";

if(!isset($_SESSION['user_id']))
{
	header('Location: login.php');
}

$upms = getUpms($_SESSION['user_id']);

$tabUser = getAllUser();
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="css/style1.css" />
        <link rel="icon" type="image/png" href="img/favicon.png" />
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" type="text/css" />
		
		<!-- jQuery and jQuery UI (REQUIRED) -->
			<link rel="stylesheet" type="text/css" media="screen" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/themes/smoothness/jquery-ui.css">
			<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
			<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>

			<!-- elFinder CSS (REQUIRED) -->
			<link rel="stylesheet" type="text/css" media="screen" href="css/elfinder.min.css">
			<link rel="stylesheet" type="text/css" media="screen" href="css/theme.css">

			<!-- elFinder JS (REQUIRED) -->
			<script type="text/javascript" src="js/elfinder.min.js"></script>

			<!-- elFinder translation (OPTIONAL) -->
			<script type="text/javascript" src="js/i18n/elfinder.ru.js"></script>

			<!-- elFinder initialization (REQUIRED) -->
			<script type="text/javascript" charset="utf-8">
				$().ready(function() {
							
				
					var myCommands = elFinder.prototype._options.commands;
					var disabled = ['extract', 'archive', 'help', 'resize', 'quicklook', 'duplicate', 'home', 'up', 'info','sort', 'view'];
					$.each(disabled, function(i, cmd) {
						(idx = $.inArray(cmd, myCommands)) !== -1 && myCommands.splice(idx,1);
					});
				
					var elf = $('#elfinder').elfinder({
						url : 'Elfinder/connector.php',  // connector URL (REQUIRED)
						width : '900',
						lang: 'fr' ,         
						commands : myCommands,
						uiOptions : {
							toolbar : [
								['back', 'forward'],
								['home'],
								['mkdir', 'mkfile', 'upload'],
								['open', 'download'],
								['copy', 'cut', 'paste'],
								['rm'],
								['search'],
							]},
					}).elfinder('instance');
				});
			</script>
			
		<title>Fichiers</title>
</head>

	
<body id="corpus">

	<?php include "include/header.php";?>

	<div class="row-fluid">
		<div class="span12">

	
			<div id="corp" style="font-family:champagne;color:white;">
				
				<div id="mainContent">
					
					<h3> Taille maximale en upload : <?php echo $upms; ?>Mo.</h3>
					<!-- Fenêtre de gestion de fichiers -->
					<div style="margin:0px auto 0px auto; max-width:940px;" id="elfinder"></div>
				
					
					<h2 style="cursor:pointer"> Génerer un lien <img src="img/add_link.png" width="40"/></h2>
					
					<section id="generateurLien" >
						
						<form enctype="multipart/form-data" action="setLink.php" method="post" >
							Privé<input type="radio" name="typeLien" value="prive" onclick='document.getElementById("selectUser").style.display="inline";'/>
							Public<input type="radio" name="typeLien" value="public" onclick='document.getElementById("selectUser").style.display="none";' />
						
							<p>Nom complet du fichier (avec extension) : <input type="text" size="50" name="url" /></p>
							
							<select name="mailUserCible" id='selectUser' style="display:none" size="5">
								<option value="0" >Choisissez un Utilisateur</option>
							<?php
								foreach ($tabUser as $cle => $valeur)
								{
									echo '<option value="'.$valeur["mail_user"].'" >'.$valeur["mail_user"].'</option>';
								}
								?>
								
							</select>
			
							<p><input class="typeButton" type="submit" value="Générer lien" name="sendNewLink" /></p>
						</form>
						
					</section>
				
				</div>
				
			</div>
			
	</div>

<br></br><br></br><br></br>

<footer>
	<?php include "include/footer.php";?>
</footer>

</body>

</html>