<?php
session_start();

if(isset($_GET['exit']) && $_GET['exit']=="logout" && isset($_SESSION['user_id'])){unset($_SESSION['user_id']);}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="css/style1.css" />
        <link rel="icon" type="image/png" href="img/favicon.png" />
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" type="text/css" />
		<title>CubbyHole</title>
</head>

	
<body id="corpus">
	 <?php include "include/header.php";?>
	
			<div class="row-fluid">
				<div class="span11 offset1">
					<h1>CubbyHole c'est tout ce qu'il vous faut!</h1>
				</div>
			</div>

			<br><br>
		
		<div class="row-fluid" style="padding:1%;">
			<div class="span4">
				<h1>Qu'est-ce que c'est?</h1>
				<img src="img/logo/Drive_Upload.png" alt="logofree" style="float: left;"/>
				<p style="padding-left: 10%; padding-right: 10%">CubbyHole est une plateforme de stockage de fichiers en ligne. Envoyez, téléchargez, partagez tous vos fichiers très simplement à l'aide de notre plateforme.</p>
			</div>

			<div class="span4">
				<h1>Sécurité et intégrité</h1>
				<img src="img/logo/Antivirus.png" alt="logosecu" style="float: left;"/>
				<p style="padding-left: 10%; padding-right: 10%">Stockez avec confiance tous vos fichiers et vos dossiers sur le cloud avec CubbyHole, nous vous garantissons un stockage efficace pour protéger vos données sensibles.</p>
			</div>

			<div class="span4">
				<h1>Application mobile</h1>
				<img src="img/logo/Phone_settings.png" alt="logosecu" style="float: left;"/>
				<p style="padding-left: 10%; padding-right: 10%">CubbyHole est également disponible en application Android, afin de vous permettre d'emporter et de gérer vos fichier à partir de n'importe où et très facilement.</p>
			</div>
		</div>

		<?php if(!isset($_SESSION['user_id']))
		{
			echo '<div class="row-fluid" style="padding:1%;">
			<div class="span3 offset2">
				<a href="inscription.php"><button class="btn btn-large">S\'incrire</button></a>
			</div>

			<div class="span3 offset3">
				<a href="login.php"><button class="btn btn-large">Se connecter</button></a>
			</div>
		</div>';
		}

		?>

			<br></br><br></br><br></br>
			<p class="lienAncre" style="text-align:center;font-size:12px;"></p>
			
			
			<hr width="90%" style="color:#96d5fc; margin-left:3%;">
		

		
		<footer>
			<?php include "include/footer.php";?>
		</footer>

</body>

</html>