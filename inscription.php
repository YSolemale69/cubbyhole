
<?php
session_start();
include "include/connexion.php";

//si déjà connecté
if(isset($_SESSION['user_id']))
{
	header('Location: index.php');
}	

$var = 0;



if(isset($_POST['sendRegister']))
{  
	
	$logMessage="";

	if($_POST['password_user'] == $_POST['password_user_conf'])
	{ 
		if(strlen($_POST['password_user']) > 5)
		{
			
			include "include/sql.php";

			$isHere=getUserByMail($_POST['email_user']);	
			
			if($isHere == 1)
			{
				$logMessage="Désolé, l'adresse email rentrée est déja asociée à un compte.";
			}
			else
			{
				setInscription($_POST['email_user'], $_POST['password_user']);
				
				//création des dossier user
				mkdir("data1/".$_SESSION['user_id']."-".$_SESSION['user_mail'], 0700);
				mkdir("data2/".$_SESSION['user_id']."-".$_SESSION['user_mail'], 0700);
				
				$logMessage="Votre compte a bien été créé avec l'adresse : ".$_POST['email_user'];
				$var=1;

				header('Location: plan.php');
			}
		}
		else
		{
			$logMessage="Le mot de passe doit contenir minimum 6 caractères";
			$var=0;
		}
	}
	else
	{
		$logMessage="Les mots de passe ne correspondent pas.";
		$var=0;
	}

}

?>

<?php

if($var==0)
{

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="css/style1.css" />
        <link rel="icon" type="image/png" href="img/favicon.png" />
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" type="text/css" />
		<title>Inscription</title>
</head>

	
<body id="corpus">
	<?php include "include/header.php";?>
	<div class="row-fluid">
		<h2 style="color:red;"><?php if(isset($logMessage)) echo $logMessage;?></h2>

		<div class="span4 offset4"> 

				<h2>Inscription</h2>
				<form enctype="multipart/form-data" action="inscription.php" method="post">
					<p><input type="email" name="email_user" placeholder="E-mail" required /></p>
					<p><input type="password" name="password_user" placeholder="Mot de passe" required></p>
					<p><input type="password" name="password_user_conf" placeholder="Confirmation"  required/></p>
					<p><button type="submit" class="btn btn-success" name="sendRegister" value="Valider"> Valider</button></p>
				</form>
				Déjà inscrit? <a href="login.php"> cliquez ici </a>

		</div>
	</div>

<br><br>

<footer>
	<?php include "include/footer.php";?>
</footer>

</body>

</html>


<?php

}
else
{

?>


<h2 style="color:#292929;"><?php if(isset($logMessage)) echo $logMessage;?></h2>

<a href="index.php">retour a la page d'accueil</a>


<?php

}

?>